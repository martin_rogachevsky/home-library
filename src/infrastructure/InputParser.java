package infrastructure;

/**
 * Helper class for parsing person's input.
 */
public final class InputParser {

    /**
     * Getting action from input passed as {@param input}.
     * @return - action.
     */
    public static String getAction(String input){
        return input.split(";")[0].trim();
    }

    /**
     * Getting arguments from input passed as {@param input}.
     * @return - array of arguments.
     */
    public static String[] getArgs(String input){
        return input.substring(input.indexOf(";")+1).trim().split(";");
    }

    /**
     * Getting login from input passed as {@param input}.
     * @return - login.
     */
    public static String getLogin(String input){
        return input.split(";")[0].trim();
    }

    /**
     * Getting password from input passed as {@param input}.
     * @return - password.
     */
    public static String getPassword(String input){
        return input.split(";")[1].trim();
    }
}
