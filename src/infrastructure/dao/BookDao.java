package infrastructure.dao;

import domain.Book;
import domain.BookGenre;
import infrastructure.TextFileDao;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.util.ArrayList;
import java.util.Comparator;

/**
 * Singleton {@code BookDao} is extension of {@code TextFileDao<>} for domain class {@code Book}.
 */
public final class BookDao extends TextFileDao<Book> {

    /**
     * Helper class for providing searching competition between books.
     */
    private class BookWrapper{
        Book book;
        Object value;

        BookWrapper(Book book, Object value){
            this.value = value;
            this.book= book;
        }
    }

    /**
     * Singleton instance.
     */
    private static BookDao instance;

    private BookDao(){

    }

    /**
     * Method for getting {@code BookDao} singleton instance.
     * @return - instance.
     */
    public static BookDao getInstance(){
        if (instance == null)
            instance = new BookDao();
        return instance;
    }

    /**
     * Method for reading book from {@param bufferedReader} that represents reader for datafile.
     * @return is a book that was read.
     * @throws Exception can be caused by work with {@code bufferedReader}
     */
    @Override
    protected Book read(BufferedReader bufferedReader) throws Exception {
        Book book = new Book();
        book.id = Integer.parseInt(bufferedReader.readLine().trim());
        book.author = bufferedReader.readLine();
        book.title = bufferedReader.readLine();
        book.genre = BookGenre.valueOf(bufferedReader.readLine());
        book.isEbook = Boolean.valueOf(bufferedReader.readLine());
        return book;
    }

    /**
     * Method for writing {@param item} to {@param bufferedWriter} that represents writer for datafile.
     * @throws Exception can be caused by work with {@code bufferedWriter}
     */
    @Override
    protected void write(Book item, BufferedWriter bufferedWriter) throws Exception {
        bufferedWriter.write(Integer.toString(item.id));
        bufferedWriter.newLine();
        bufferedWriter.write(item.author);
        bufferedWriter.newLine();
        bufferedWriter.write(item.title);
        bufferedWriter.newLine();
        bufferedWriter.write(item.genre.toString());
        bufferedWriter.newLine();
        bufferedWriter.write(item.isEbook.toString());
        bufferedWriter.newLine();
    }

    /**
     * Searching books by parameters.
     * Put {@code null} as parameter if actual value is unknown.
     * @param title - book title.
     * @param author - book author.
     * @param genre - book genre
     * @param isEbook - is book eBook ({@code true}) of not ({@code false}).
     * @return
     */
    public ArrayList<Book> findBook(String title, String author, BookGenre genre, Boolean isEbook){
        ArrayList<BookWrapper> list = new ArrayList<BookWrapper>();
        for (Book book: set) {
            int score = 0;
            if (title != null)
                if (book.title.toLowerCase() == title.toLowerCase() || book.title.toLowerCase().contains(title.toLowerCase()))
                    score = 4;
            if (author != null)
                if (book.author.toLowerCase() == author.toLowerCase() || book.author.toLowerCase().contains(author.toLowerCase()))
                    score += 3;
            if (genre != null)
                if (book.genre == genre)
                    score += 2;
            if (isEbook != null)
                if (book.isEbook == isEbook)
                    score ++;
            if (score > 0)
                list.add(new BookWrapper(book, score));
            list.sort(Comparator.comparing(a -> (int)a.value));
        }
        ArrayList<Book> result = new ArrayList<Book>();
        for(BookWrapper bookWrapper: list){
            result.add(bookWrapper.book);
        }
        return result;
    }
}
