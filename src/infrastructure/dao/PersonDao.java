package infrastructure.dao;

import domain.Person;
import domain.PersonRole;
import infrastructure.TextFileDao;

import java.io.BufferedReader;
import java.io.BufferedWriter;

/**
 * Singleton {@code PersonDao} is extension of {@code TextFileDao<>} for domain class {@code Person}.
 */
public final class PersonDao extends TextFileDao<Person>{

    /**
     * Singleton instance.
     */
    private static PersonDao instance;

    private PersonDao(){

    }

    /**
     * Method for getting {@code PersonDao} singleton instance.
     * @return - instance.
     */
    public static PersonDao getInstance(){
        if (instance == null){
            instance = new PersonDao();
        }
        return instance;
    }

    /**
     * Method for reading person from {@param bufferedReader} that represents reader for datafile.
     * @return is a book that was read.
     * @throws Exception can be caused by work with {@code bufferedReader}
     */
    @Override
    protected Person read(BufferedReader bufferedReader) throws Exception{
        Person person = new Person(PersonRole.User);
        person.id = Integer.parseInt(bufferedReader.readLine());
        person.name = bufferedReader.readLine();
        person.email = bufferedReader.readLine();
        person.password = bufferedReader.readLine();
        if (bufferedReader.readLine().equalsIgnoreCase( "A")) person.setRole(PersonRole.Administrator);
        return person;
    }

    /**
     * Method for writing {@param item} to {@param bufferedWriter} that represents writer for datafile.
     * @throws Exception can be caused by work with {@code bufferedWriter}
     */
    @Override
    protected void write(Person person, BufferedWriter bufferedWriter) throws Exception{
        bufferedWriter.write(Integer.toString(person.id));
        bufferedWriter.newLine();
        bufferedWriter.write(person.name);
        bufferedWriter.newLine();
        bufferedWriter.write(person.email);
        bufferedWriter.newLine();
        bufferedWriter.write(person.password);
        bufferedWriter.newLine();
        if (person.getRole() == PersonRole.Administrator) bufferedWriter.write("A");
        else bufferedWriter.write("NA");
        bufferedWriter.newLine();
    }

    /**
     * Method for searching exact person by {@param name} and {@param password} (encrypted value).
     * @return is instance of {@code Person} is person was found. Otherwise value is {@code null}.
    */
    public Person getPerson(String name, String password){
        for (Person person: set) {
            if (person.name.equals(name) && person.password.equals(password))
                return person;
        }
        return null;
    }

    /**
     * Method for finding out whether person with name {@param name} exists or not.
     * {@return == true} if person exists.
     * {@return == false} if person doesn't exist.
     */
    public boolean personExists(String name){
        for (Person person: set) {
            if (person.name.equals(name))
                return true;
        }
        return false;
    }
}
