package infrastructure.interfaces;

/**
 * Interface for views.
 * Every class, that implements the interface, should have ability to show, build and clean the view.
 */
public interface IView {

    /**
     * Method shows view and retrieves user input.
     * @return - user input.
     */
    String show();

    /**
     * Method that allows view to get data from outside.
     * @param param - data that used by view.
     */
    void build(Object param);

    /**
     * Method that resets view.
     */
    void clean();
}
