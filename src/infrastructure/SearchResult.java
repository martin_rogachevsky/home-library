package infrastructure;

import domain.Book;

import java.util.ArrayList;

/**
 * Wrapper class for searching books result.
 */
public class SearchResult {

    /**
     * Collection of books sorted by search match accuracy.
     */
    public ArrayList<Book> result;

    public SearchResult(ArrayList<Book> books){
        result = books;
    }
}
