package infrastructure;

import domain.Item;
import infrastructure.interfaces.IDao;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.Collection;
import java.util.HashSet;

/**
 * Abstract class that implements {@code IDao} interface for reading and writing data from/to text file.
 * Implement {@code write()} and {@code read()} methods for using {@code TextFileDao<>}
 * @param <T> - type of domain class, that extends {@code Item} class.
 */
public abstract class TextFileDao<T extends Item> implements IDao<T> {

    /**
     * Collection of items, that represents file condition.
     */
    protected Collection<T> set;

    /**
     * Data file name.
     */
    private String fileName;

    /**
     * Method for reading items from {@param bufferedReader} that represents reader for datafile.
     * @return is a item that was read.
     * @throws Exception can be caused by work with {@code bufferedReader}
     */
    protected abstract T read(BufferedReader bufferedReader) throws Exception;

    /**
     * Method for writing {@param item} to {@param bufferedWriter} that represents writer for datafile.
     * @throws Exception can be caused by work with {@code bufferedWriter}
     */
    protected abstract void write(T item, BufferedWriter bufferedWriter) throws Exception;

    /**
     * Connection to the file.
     *
     * @param filename - name of the file that consists data.
     * After opening connection method will return {@return} configured DAO class.
     * @throws Exception can be caused by filesystem errors.
     */
    @Override
    public IDao openConnection(String filename) throws Exception{
        fileName = filename;
        set = new HashSet<>();
        try (FileReader fileReader = new FileReader(filename);
             BufferedReader bufferedReader = new BufferedReader(fileReader)) {
            int count = Integer.parseInt(bufferedReader.readLine().trim());
            for (int i = 0; i < count; i++){
                set.add(read(bufferedReader));
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return this;
    }

    /**
     * Saving data to the file.
     */
    @Override
    public void save(){
        try (FileWriter fileWriter = new FileWriter(fileName);
             BufferedWriter bufferedWriter = new BufferedWriter(fileWriter)) {
            bufferedWriter.write(Integer.toString(set.size()));
            bufferedWriter.newLine();
            for (T item: set){
                write(item, bufferedWriter);
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * Retrieving all data from DAO.
     *
     * @return is a certain class HashSet of objects.
     */
    @Override
    public Collection<T> getAll(){
        return set;
    }

    /**
     * Getting object with {@param id} passed as parameter.
     *
     * @return is a required object.
     */
    @Override
    public T get(int id){
        for(T item: set){
            if (item.id == id) return item;
        }
        return null;
    }

    /**
     * Method for adding items to DAO.
     * Item is passing as parameter{@param item}.
     *
     * @return - id of added item.
     */
    @Override
    public int add(T item){
        int id = 0;
        for(T setItem: set){
            if (setItem.id >= id) id = setItem.id + 1;
        }
        item.id = id;
        set.add(item);
        save();
        return id;
    }

    /**
     * Removing item from DAO.
     * @param id - id of removing item.
     * @return - id of removed item.
     */
    @Override
    public int remove(int id){
        set.removeIf(i -> i.id == id);
        save();
        return id;
    }

    /**
     * Updating item.
     * @param item - item that supposed to be updated.
     * @return - id of updated item.
     */
    @Override
    public int update(T item) {
        set.removeIf(i -> i.id == item.id);
        set.add(item);
        save();
        return item.id;
    }
}
