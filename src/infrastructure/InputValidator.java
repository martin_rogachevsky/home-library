package infrastructure;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Helper class for validation person's input.
 */
public final class InputValidator{

    /**
     * Checking whether login passed as {@param login} is correct or not.
     * {@return == true} if login is valid.
     * {@return == false} if login is invalid.
     */
    public static boolean isLoginValid(String login){
        return !(login == null || login.trim().length() == 0 || login.contains(" ") || login.contains(";")
                || login.contains(",") || login.contains("/") || login.contains("\"") || login.contains("\'")
                || login.contains("\\"));
    }

    /**
     * Checking whether password passed as {@param password} is correct or not.
     * {@return == true} if password is valid.
     * {@return == false} if password is invalid.
     */
    public static boolean isPasswordValid(String password){
        return !(password == null || password.trim().length() == 0 || password.contains(" ") || password.contains(";"));
    }

    /**
     * Checking whether email passed as {@param email} is correct or not.
     * {@return == true} if email is valid.
     * {@return == false} if email is invalid.
     */
    public static boolean isEmailValid(String email) {
        Matcher matcher = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE).matcher(email);
        return matcher.find();
    }

    /**
     * Checking whether sign up arguments passed as {@param args} are correct or not.
     * {@return == true} if arguments are valid.
     * {@return == false} if arguments are invalid.
     */
    public static String checkSignUpInput(String[] args){
        if (args.length != 5)
            return "Some data is missed!";
        if (!isLoginValid(args[0]))
            return "Login is invalid!";
        if (!isPasswordValid(args[1]))
            return "Password is invalid!";
        if (!args[1].contentEquals(args[2]))
            return "Passwords don't match!";
        if (!isEmailValid(args[3]))
            return "Email is not valid";
        return null;
    }
}
