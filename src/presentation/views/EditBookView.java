package presentation.views;

import domain.Book;
import domain.BookGenre;
import infrastructure.interfaces.IView;

import java.util.Scanner;

/**
 * Edit book view class.
 */
public final class EditBookView implements IView{
    private Book editBook;

    /**
     * Method shows view and retrieves user input.
     * @return - user input.
     */
    public String show() {
        Scanner sc = new Scanner(System.in);

        System.out.println("------------------------------------------");
        System.out.println("Book editing.");
        System.out.println("Leave all fields empty to come back.");


        System.out.println("------------------------------------------");
        if (editBook != null){
            System.out.println("Current title: " + editBook.title);
            System.out.println("Leave field empty to save current value.");
            System.out.println("------------------------------------------");
        }
        System.out.print("Enter title: ");
        String title = sc.nextLine();
        System.out.println("------------------------------------------");
        if (editBook != null){
            System.out.println("Current author: " + editBook.author);
            System.out.println("Leave field empty to save current value.");
            System.out.println("------------------------------------------");
        }
        System.out.print("Enter author: ");
        String author = sc.nextLine();

        System.out.println("------------------------------------------");
        if (editBook != null){
            System.out.println("Current title: " + editBook.genre.toString());
            System.out.println("Leave field empty to save current value.");
            System.out.println("------------------------------------------");
        }
        System.out.println("Available genres:");
        System.out.println(BookGenre.Comedy.toString());
        System.out.println(BookGenre.Drama.toString());
        System.out.println(BookGenre.Fantasy.toString());
        System.out.println(BookGenre.HorrorFiction.toString());
        System.out.println(BookGenre.LiteraryRealism.toString());
        System.out.println(BookGenre.Mythology.toString());
        System.out.println(BookGenre.Romance.toString());
        System.out.println(BookGenre.Satire.toString());
        System.out.println(BookGenre.Technology.toString());
        System.out.println(BookGenre.Tragedy.toString());
        System.out.println(BookGenre.Tragicomedy.toString());
        System.out.println("------------------------------------------");
        System.out.print("Enter genre: ");
        String genre = sc.nextLine();

        System.out.println("------------------------------------------");
        if (editBook != null){
            System.out.println("Current value: "+(editBook.isEbook?"Ebook":"Paper"));
            System.out.println("Leave field empty to save current value.");
            System.out.println("------------------------------------------");
        }
        System.out.print("Is that a EBook? (y/n): ");
        String material = sc.nextLine();

        if (title.trim().length() == 0 && author.trim().length() == 0 && genre.trim().length() == 0
                && material.trim().length() == 0)
            return "back";

        return ((editBook == null)?"new":"edit")+";"+title+";"+author+";"+genre+";"
                +material+((editBook == null)?"":";"+editBook.id);
    }

    /**
     * Method that allows view to get data from outside.
     * Pass {@code Book} as {@param param} to edit book.
     * Pass {@code null} as {@param param} to create new book.
     */
    @Override
    public void build(Object param) {
        if (param != null && param instanceof Book)
            editBook = (Book) param;
    }

    /**
     * Method that resets view.
     */
    @Override
    public void clean() {
        editBook = null;
    }
}
