package presentation.views;

import domain.Person;
import infrastructure.interfaces.IView;

import java.util.Scanner;

/**
 * Menu view class.
 */
public final class MenuView implements IView {
    private Person person;
    private boolean isLog = false;
    private boolean loaded = false;
    private boolean loading = false;

    /**
     * Method shows view and retrieves user input.
     * @return - user input.
     */
    @Override
    public String show(){
        if (!loaded){
            loading = true;
            return "load";
        }
        loading = false;
        System.out.println("===========================");
        if (isLog){
            System.out.println(person.getRole().toString()+": "+person.name);
            System.out.println("===========================");
            System.out.println("'logout' - log out");
            System.out.println("'cat' - catalog.");
        }
        else{
            System.out.println("Please, log in.");
            System.out.println("===========================");
            System.out.println("'sign' - sign up.");
            System.out.println("'login' - log in.");
        }
        System.out.println("===========================");
        System.out.print("> ");
        Scanner sc = new Scanner(System.in);
        return sc.nextLine();
    }

    /**
     * Method that allows view to get data from outside.
     * @param param - data that used by view.
     */
    @Override
    public void build(Object param) {
        if (param == null){
            if (loading){
                loaded = true;
                loading = false;
            }
        }
        else{
            if (param instanceof Person){
                person = (Person) param;
                isLog = true;
                loaded = true;
            }
        }
    }

    /**
     * Method that resets view.
     */
    @Override
    public void clean() {
        person = null;
        isLog = false;
        loaded = false;
    }
}
