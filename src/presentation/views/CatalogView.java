package presentation.views;

import domain.Book;
import domain.Person;
import domain.PersonRole;
import infrastructure.interfaces.IView;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * Catalog view class.
 */
public final class CatalogView implements IView{
    private ArrayList<Book> shelf;
    private int page = 1;
    private int booksPerPage = 5;
    private Person currPerson;
    private boolean loading;
    private int loadingPage;

    /**
     * Method shows view and retrieves user input.
     * @return - user input.
     */
    @Override
    public String show() {
        if (shelf == null) {
            return "load;"+page+";"+booksPerPage;
        }
        Scanner sc = new Scanner(System.in);

        System.out.println("=======================================================");
        System.out.println("             Page: "+page+" ("+booksPerPage+" books on page)");
        System.out.println("=======================================================");
        if (shelf.size() == 0){
            System.out.println("                 There's no books.");
        }
        for (Book book: shelf){
            showBook(book);
        }
        System.out.println("=======================================================");

        System.out.println("'next' - next page.");
        System.out.println("'prev' - previous page.");
        System.out.println("'topage' - to certain page.");
        System.out.println("'chamount' - change amount of books per page (default - 5).");
        if (currPerson.getRole() == PersonRole.Administrator){
            System.out.println("'edit' - edit a book.");
            System.out.println("'rm' - remove a book.");
            System.out.println("'add' - add a book.");
        }

        System.out.println("'find' - books search.");
        System.out.println("'back' - come back.");

        System.out.print("> ");
        return actionReflection(sc.nextLine(), sc);
    }

    /**
     * Method shows book passed as {@param book} information.
     */
    static void showBook(Book book) {
        System.out.println("-------------------------------------------------------");
        System.out.println("Title: "+ book.title+ " (ID = "+book.id+")");
        System.out.println("Author: "+ book.author);
        System.out.println("Genre: "+ book.genre.toString());
        System.out.println("EBook/Paper: "+(book.isEbook?"Ebook":"Paper"));
        System.out.println("-------------------------------------------------------");
    }

    private String actionReflection(String action, Scanner sc){
        switch (action.trim().toLowerCase()){
            case "next":
                loading = true;
                loadingPage = page+1;
                return "load;"+loadingPage+";"+booksPerPage;
            case "prev":
                loading = true;
                loadingPage = page-1;
                return "load;"+loadingPage+";"+booksPerPage;
            case "topage":
                System.out.print("Page #");
                String input = sc.nextLine().trim();
                try {
                    loadingPage = Integer.parseInt(input);
                    loading = true;
                }catch (Exception e){
                    return "load;"+input+";"+booksPerPage;
                }
                return "load;"+loadingPage+";"+booksPerPage;
            case "chamount":
                System.out.print("Amount of books at one page: ");
                return action+";"+sc.nextLine();
            case "edit":
            case "rm":
                System.out.print("Enter book ID: ");
                return action+";"+sc.nextLine();
            default:
                return action;
        }
    }

    /**
     * Method that allows view to get data from outside.
     * Pass {@code ArrayList<Book>} as {@param param} to show set of books.
     * Pass {@code Person} as {@param param} to set current person.
     * Pass {@code Integer} as {@param param} to set amount books per page.
     */
    @Override
    public void build(Object param) {
        if (param != null){
            if (param instanceof ArrayList) {
                if (loading)
                    page = loadingPage;
                shelf = (ArrayList<Book>) param;
            }
            else if (param instanceof Person){
                currPerson = (Person) param;
            }
            else if (param instanceof Integer){
                booksPerPage = (Integer)param;
            }
        }

    }

    /**
     * Method that resets view.
     */
    @Override
    public void clean() {
        shelf = null;
        page = 1;
    }
}
