package presentation.views;

import domain.Book;
import infrastructure.SearchResult;
import infrastructure.interfaces.IView;
import java.util.Collection;
import java.util.Scanner;

/**
 * Search result view class.
 */
public final class SearchRelustView implements IView{
    private Collection<Book> shelf;

    /**
     * Method shows view and retrieves user input.
     * @return - user input.
     */
    @Override
    public String show() {
        Scanner sc = new Scanner(System.in);
        System.out.println("=======================================================");
        if (shelf.size() == 0){
            System.out.println("                 There's no books.");
        }
        for (Book book: shelf){
            CatalogView.showBook(book);
        }
        System.out.println("=======================================================");

        System.out.println("'back' - come back.");

        System.out.print("> ");
        return sc.nextLine();

    }

    /**
     * Method that allows view to get data from outside.
     * @param param - data that used by view.
     */
    @Override
    public void build(Object param) {
        if (param != null && param instanceof SearchResult){
            shelf = ((SearchResult)param).result;
        }

    }

    /**
     * Method that resets view.
     */
    @Override
    public void clean() {
        shelf = null;
    }
}
