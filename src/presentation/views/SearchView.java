package presentation.views;

import domain.BookGenre;
import infrastructure.interfaces.IView;

import java.util.Scanner;

/**
 * Search engine veiw class.
 */
public final class SearchView implements IView {

    /**
     * Method shows view and retrieves user input.
     * @return - user input.
     */
    @Override
    public String show() {
        Scanner sc = new Scanner(System.in);

        System.out.println("------------------------------------------");
        System.out.println("Book searching.");
        System.out.println("Leave all fields empty to come back.");
        System.out.println("Fill only fields that you surely know.");

        System.out.println("------------------------------------------");
        System.out.print("Enter title: ");
        String title = sc.nextLine();
        System.out.println("------------------------------------------");
        System.out.print("Enter author: ");
        String author = sc.nextLine();
        System.out.println("------------------------------------------");
        System.out.println("Available genres:");
        System.out.println(BookGenre.Comedy.toString());
        System.out.println(BookGenre.Drama.toString());
        System.out.println(BookGenre.Fantasy.toString());
        System.out.println(BookGenre.HorrorFiction.toString());
        System.out.println(BookGenre.LiteraryRealism.toString());
        System.out.println(BookGenre.Mythology.toString());
        System.out.println(BookGenre.Romance.toString());
        System.out.println(BookGenre.Satire.toString());
        System.out.println(BookGenre.Technology.toString());
        System.out.println(BookGenre.Tragedy.toString());
        System.out.println(BookGenre.Tragicomedy.toString());
        System.out.println("------------------------------------------");
        System.out.print("Enter genre: ");
        String genre = sc.nextLine();
        System.out.println("------------------------------------------");
        System.out.print("Is that a EBook? (y/n): ");
        String material = sc.nextLine();

        if (title.trim().length() == 0 && author.trim().length() == 0 && genre.trim().length() == 0
                && material.trim().length() == 0)
            return "back";

        return "find;"+title+";"+author+";"+genre+";"+material;
    }

    /**
     * Method that allows view to get data from outside.
     * @param param - data that used by view.
     */
    @Override
    public void build(Object param) {

    }

    /**
     * Method that resets view.
     */
    @Override
    public void clean() {

    }
}
