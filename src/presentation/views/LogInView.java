package presentation.views;

import infrastructure.interfaces.IView;

import java.util.Scanner;

/**
 * Log in view class.
 */
public final class LogInView implements IView{
    private String message;

    /**
     * Method shows view and retrieves user input.
     * @return - user input.
     */
    @Override
    public String show(){
        Scanner sc = new Scanner(System.in);
        System.out.println("-----------------------------------------------------------");
        if (message != null){
            System.out.println(message);
        }
        System.out.println("Leave both 'login' and 'password' section empty to escape.");
        System.out.println("-----------------------------------------------------------");

        System.out.print("login: ");
        String login = sc.nextLine();
        System.out.print("password: ");
        String password = sc.nextLine();

        if (login.trim().length() == 0 && password.trim().length() == 0)
            return "back";

        return login+";"+password;
    }

    /**
     * Method that allows view to get data from outside.
     * @param param - data that used by view.
     */
    @Override
    public void build(Object param) {
        if (param != null && param instanceof String)
            message = (String) param;
    }

    /**
     * Method that resets view.
     */
    @Override
    public void clean() {
        message = null;
    }
}
