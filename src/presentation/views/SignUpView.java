package presentation.views;

import domain.Person;
import domain.PersonRole;
import infrastructure.interfaces.IView;

import java.util.Scanner;

/**
 * Sign up view class.
 */
public final class SignUpView implements IView{
    private Person registrator;
    private PersonRole chosenRole;

    /**
     * Method shows view and retrieves user input.
     * @return - user input.
     */
    @Override
    public String show() {
        Scanner sc = new Scanner(System.in);

        if (chosenRole == null && registrator == null){
            System.out.println("Choose the role.");
            System.out.println("'adm' - administrator.");
            System.out.println("'usr' - user.");
            System.out.print("role: ");
            String role = sc.nextLine();
            return "regrole;"+role+((registrator != null)?";"+registrator.id:"");
        }

        System.out.println("-----------------------------------------------------------");
        System.out.println("Creating "+((chosenRole == null)?registrator.getRole().toString():chosenRole.toString())+".");
        System.out.println("Leave all of the sections empty to escape.");
        System.out.println("-----------------------------------------------------------");

        System.out.print("login: ");
        String login = sc.nextLine();
        System.out.print("password: ");
        String password = sc.nextLine();
        System.out.print("repeat password: ");
        String passwordRp = sc.nextLine();
        System.out.print("email: ");
        String email = sc.nextLine();

        if (login.trim().length() == 0 && password.trim().length() == 0 && passwordRp.trim().length() == 0
                && email.trim().length() == 0)
            return "back";

        return "reg;"+login+";"+password+";"+passwordRp+";"+email+
                ";"+((chosenRole == null)?registrator.getRole().toString():chosenRole.toString());
    }

    /**
     * Method that allows view to get data from outside.
     * @param param - data that used by view.
     */
    @Override
    public void build(Object param) {
        if (param != null)
            if (param instanceof Person)
                registrator = (Person)param;
            else if (param instanceof PersonRole)
                chosenRole = (PersonRole)param;
    }

    /**
     * Method that resets view.
     */
    @Override
    public void clean() {
        registrator = null;
        chosenRole = null;
    }
}
