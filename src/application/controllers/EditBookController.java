package application.controllers;

import domain.Book;
import domain.BookGenre;
import infrastructure.InputParser;
import infrastructure.Response;
import infrastructure.ResponseCode;
import infrastructure.dao.BookDao;
import infrastructure.interfaces.IController;

/**
 * Controller that handles commands from edit book view.
 */
public final class EditBookController implements IController {

    /**
     * Method for handling a command, that comes as {@param command}.
     * Method returns {@return} response that can be handled by View-system.
     */
    @Override
    public Response handleCommand(String command) {
        switch (InputParser.getAction(command)){
            case "new":
                return newBook(InputParser.getArgs(command));
            case "edit":
                return editBook(InputParser.getArgs(command));
            case "back":
                return back();
            default:
                return new Response(ResponseCode.Fail, null, "There is no handler for "
                        +InputParser.getAction(command)+" action!");
        }
    }

    private Response newBook(String[] args){
        BookDao dao = BookDao.getInstance();
        Book book = new Book();
        if (args.length < 4)
            return new Response(ResponseCode.Fail, null, "To create new book you should " +
                    "enter all necessary data!");
        book.title = args[0];
        book.author = args[1];
        try{
            book.genre = BookGenre.valueOf(args[2]);
        } catch(Exception e) {
            return new Response(ResponseCode.Fail, null, "Unknown book genre - "+args[2]+"! " +
                    "Use only available genres!");
        }
        if (!args[3].trim().equalsIgnoreCase("y") && !args[3].trim().equalsIgnoreCase("n"))
            return new Response(ResponseCode.Fail, null, "For book type only 'y' or 'n' is valid!");
        book.isEbook = args[3].trim().equalsIgnoreCase("y");

        dao.add(book);

        return new Response(ResponseCode.Back, null, null);
    }

    private Response editBook(String[] args){
        BookDao dao = BookDao.getInstance();
        Book book = dao.get(Integer.parseInt(args[4]));
        if (book == null)
            return new Response(ResponseCode.Fail, null, "There is no book with id="+args[4]+"!");

        if (!args[3].trim().equalsIgnoreCase("y") && !args[3].trim().equalsIgnoreCase("n"))
            return new Response(ResponseCode.Fail, null, "For book type only 'y' or 'n' is valid!");
        if (args[2].trim().length() !=0)
            try{
                book.genre = BookGenre.valueOf(args[2]);
            } catch(Exception e) {
                return new Response(ResponseCode.Fail, null, "Unknown book genre - "+args[2]+"! " +
                        "Use only available genres!");
            }
        book.isEbook = args[3].trim().equalsIgnoreCase("y");
        if (args[0].trim().length() != 0)
            book.title = args[0];
        if (args[1].trim().length() != 0)
            book.author = args[1];
        dao.update(book);

        return new Response(ResponseCode.Back, null, null);
    }

    /**
     * Method to handle 'come-back' type of command.
     *
     * @return is the response that means coming back.
     */
    @Override
    public Response back() {
        return new Response(ResponseCode.Back, null, null);
    }
}
