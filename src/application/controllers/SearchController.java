package application.controllers;

import domain.BookGenre;
import infrastructure.InputParser;
import infrastructure.Response;
import infrastructure.ResponseCode;
import infrastructure.SearchResult;
import infrastructure.dao.BookDao;
import infrastructure.interfaces.IController;

/**
 * Controller that handles commands from search view.
 */
public final class SearchController implements IController{

    /**
     * Method for handling a command, that comes as {@param command}.
     * Method returns {@return} response that can be handled by View-system.
     */
    @Override
    public Response handleCommand(String command) {
        switch (InputParser.getAction(command)){
            case "find":
                return find(InputParser.getArgs(command));
            case "back":
                return back();
            default:
                return new Response(ResponseCode.Fail, null, "There is no handler for "
                        +InputParser.getAction(command)+" action!");
        }
    }

    private Response find(String[] args){
        String title = null, author = null;
        BookGenre genre = null;
        Boolean isEbook = null;
        if (args[0].trim().length()>0)
            title = args[0];
        if (args.length >= 2 && args[1].trim().length()>0)
            author = args[1];
        if (args.length >= 3 &&args[2].trim().length()>0) {
            try{
                genre = BookGenre.valueOf(args[2]);
            } catch(Exception e) {
                return new Response(ResponseCode.Fail, null, "Unknown book genre - "+args[2]+"! " +
                        "Use only available genres!");
            }
        }
        if (args.length == 4 && args[3].trim().length()>0) {
            if (!args[3].trim().equalsIgnoreCase("y") && !args[3].trim().equalsIgnoreCase("n"))
                return new Response(ResponseCode.Fail, null, "For book type only 'y' or 'n' is valid!");
            isEbook = args[3].trim().equalsIgnoreCase("y");
        }
        return new Response(ResponseCode.Redir, "searchResult",
                new SearchResult(BookDao.getInstance().findBook(title,author,genre,isEbook)));
    }

    /**
     * Method to handle 'come-back' type of command.
     *
     * @return is the response that means coming back.
     */
    @Override
    public Response back() {
        return new Response(ResponseCode.Back, null, null);
    }
}
