package application.controllers;

import application.HomeLib;
import domain.Person;
import domain.PersonRole;
import infrastructure.*;
import infrastructure.dao.PersonDao;
import infrastructure.interfaces.IController;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;

import static domain.PersonRole.Administrator;
import static domain.PersonRole.User;

/**
 * Controller that handles commands from sign up view.
 */
public final class SignUpController implements IController {

    /**
     * Method for handling a command, that comes as {@param command}.
     * Method returns {@return} response that can be handled by View-system.
     */
    @Override
    public Response handleCommand(String command) {
        if (command == null) return new Response(ResponseCode.Fail, null, "Enter the command!");
        String action = InputParser.getAction(command);
        Response response = null;
        switch (action){
            case "reg":
                response = reg(InputParser.getArgs(command));
                break;
            case "regrole":
                response = regRole(InputParser.getArgs(command));
                break;
            case "back":
                response = back();
                break;
            default:
                return new Response(ResponseCode.Fail, null, (action + " - is not a command!"));
        }
        return response;
    }

    private Response reg(String[] args){
        String validationReport = InputValidator.checkSignUpInput(args);
        if (validationReport == null){
            PersonDao dao = PersonDao.getInstance();
            Person person = new Person(PersonRole.valueOf(args[4]));
            person.name = args[0];
            try {
                person.password =
                        new String(MessageDigest.getInstance("SHA-256").digest(args[1].getBytes(StandardCharsets.UTF_8))
                                , StandardCharsets.UTF_8);
            } catch (Exception e){
                return new Response(ResponseCode.Fail, null, "There is no SHA-256 algorithm.");
            }
            person.email = args[3];
            if (dao.getAll().isEmpty()) {
                person.setRole(Administrator);
                dao.add(person);
            }else if (!dao.personExists(person.name)){
                dao.add(person);
            }
            else return new Response(ResponseCode.Fail, null, "Person with the same login already exists!");
            HomeLib.setCurrentUser(person);
            return new Response(ResponseCode.Back, null, person);
        }
        else{
            return new Response(ResponseCode.Fail, null, validationReport);
        }
    }

    private Response regRole(String[] args){
        PersonRole role;
        if (args[0].equalsIgnoreCase("adm"))
            role = Administrator;
        else if (args[0].equalsIgnoreCase("usr"))
            role = User;
        else return new Response(ResponseCode.Fail, null, args[0]+" - not a command!");
        switch (role){
            case Administrator:
                if (args.length == 1)
                    return new Response(ResponseCode.Redir, "auth", "You need to authorize as administrator to create other administrator!");
                else{
                    PersonDao dao = PersonDao.getInstance();
                    try {
                        Person registrator = dao.get(Integer.parseInt(args[1]));
                        if (registrator == null)
                            return new Response(ResponseCode.Fail, null, "There is no person with id="+args[1]+"!");
                        else if (registrator.getRole() != Administrator)
                            return new Response(ResponseCode.Fail, null,
                                    "You need to authorize as administrator to create other administrator!");
                        return new Response(ResponseCode.Ok, null, registrator);
                    }
                    catch (Exception e){
                        return new Response(ResponseCode.Fail, null, e.getMessage());
                    }
                }
            case User:
                return new Response(ResponseCode.Ok, null, User);
            default:
                return new Response(ResponseCode.Fail, null, "Unknown person role!");
        }
    }

    /**
     * Method to handle 'come-back' type of command.
     * @return is the response that means coming back.
     */
    @Override
    public Response back() {
        return new Response(ResponseCode.Back, null, null);
    }
}
