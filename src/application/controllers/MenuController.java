package application.controllers;

import application.HomeLib;
import infrastructure.InputParser;
import infrastructure.interfaces.IController;
import infrastructure.Response;
import infrastructure.ResponseCode;

/**
 * Controller that handles commands from menu view.
 */
public final class MenuController implements IController {

    /**
     * Method for handling a command, that comes as {@param command}.
     * Method returns {@return} response that can be handled by View-system.
     */
    @Override
    public Response handleCommand(String command){
        if (command == null) return new Response(ResponseCode.Fail, null, "Enter the command!");
        String action = InputParser.getAction(command);
        Response response = null;
        switch (action){
            case "login":
                response = logIn();
                break;
            case "logout":
                response = logOut();
                break;
            case "sign":
                response = signIn();
                break;
            case "back":
                response = back();
                break;
            case "cat":
                response = cat();
                break;
            case "load":
                return new Response(ResponseCode.Ok, null, HomeLib.getCurrentUser());
            default:
                return new Response(ResponseCode.Fail, null, (action + " - is not a command!"));
        }
        return response;
    }

    /**
     * Method to handle 'come-back' type of command.
     * @return is the response that means coming back.
     */
    @Override
    public Response back() {
        return new Response(ResponseCode.Fail, null,  "Moving back from menu is impossible! Use 'quit' to finish.");
    }

    private Response cat() {
        if (HomeLib.getCurrentUser() == null)
            return new Response(ResponseCode.Fail, null, "Log in first!");
        return new Response(ResponseCode.Redir, "catalog", HomeLib.getCurrentUser());
    }

    private Response logIn(){
        if (HomeLib.getCurrentUser() != null)
            return new Response(ResponseCode.Fail, null, "Log out first!");
        return new Response(ResponseCode.Redir, "auth", null);
    }

    private Response logOut(){
        if (HomeLib.getCurrentUser() == null)
            return new Response(ResponseCode.Fail, null, "Log in first!");
        HomeLib.setCurrentUser(null);
        return new Response(ResponseCode.Ok, null, null);
    }

    private Response signIn() {
        if (HomeLib.getCurrentUser() != null)
            return new Response(ResponseCode.Fail, null, "Log out first!");
        return new Response(ResponseCode.Redir, "sign", null);
    }

}
