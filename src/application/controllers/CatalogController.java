package application.controllers;

import application.HomeLib;
import domain.Book;
import domain.PersonRole;
import infrastructure.InputParser;
import infrastructure.ResponseCode;
import infrastructure.dao.BookDao;
import infrastructure.interfaces.IController;
import infrastructure.Response;

import java.util.ArrayList;
import java.util.Comparator;

import static infrastructure.ResponseCode.Fail;
import static infrastructure.ResponseCode.Ok;
import static infrastructure.ResponseCode.Redir;

/**
 * Controller that handles commands from catalog view.
 */
public final class CatalogController implements IController{

    /**
     * Method for handling a command, that comes as {@param command}.
     * Method returns {@return} response that can be handled by View-system.
     */
    @Override
    public Response handleCommand(String command) {
        switch (InputParser.getAction(command)){
            case "load":
                return load(InputParser.getArgs(command));
            case "chamount":
                return chamount(InputParser.getArgs(command));
            case "edit":
                return edit(InputParser.getArgs(command));
            case "rm":
                return remove(InputParser.getArgs(command));
            case "add":
                return add();
            case "back":
                return back();
            case "find":
                return new Response(Redir, "search", null);
            default:
                return new Response(Fail, null, "Unknown command - "+
                        InputParser.getAction(command)+"!");
        }
    }

    private Response load (String[] args){
        if (args.length < 2)
            return new Response(Fail, null, "Loading require 2 parameters!");

        int page, bookPerPage;
        try{
            page = Integer.parseInt(args[0]);
            bookPerPage = Integer.parseInt(args[1]);
        } catch (Exception e) {
            return new Response(Fail, null, e.getMessage());
        }

        BookDao dao = BookDao.getInstance();
        ArrayList<Book> books = new ArrayList<>(dao.getAll());
        if (books.size() == 0 && page == 1)
            return new Response(Ok, null, new ArrayList<>());
        books.sort(Comparator.comparingInt(b -> b.id));

        if (books.size()/bookPerPage + (books.size()%bookPerPage == 0?0:1) < page ||
                page <= 0)
            return new Response(Fail, null, "There's no page #"+page+"!");

        return new Response(Ok, null, new ArrayList<>(books.subList(bookPerPage*(page-1),
                (books.size()/bookPerPage + (books.size()%bookPerPage == 0?0:1) != page)
                        ?(bookPerPage*(page-1)+bookPerPage)
                        :(books.size()))));
    }

    private Response chamount (String[] args){
        if (args.length != 1)
            return new Response(Fail, null, "Require one parameter!");
        int bookPerPage;
        try{
            bookPerPage = Integer.parseInt(args[0]);
        } catch (Exception e) {
            return new Response(Fail, null, e.getMessage());
        }
        return new Response(Ok, null, bookPerPage);
    }

    private Response remove (String[] args){
        if (HomeLib.getCurrentUser().getRole() != PersonRole.Administrator)
            return new Response(Fail, null, "Access denied!");
        if (args.length != 1)
            return new Response(Fail, null, "Require one parameter!");
        int id;
        try{
            id = Integer.parseInt(args[0]);
        } catch (Exception e) {
            return new Response(Fail, null, e.getMessage());
        }
        BookDao dao = BookDao.getInstance();
        if (dao.get(id) == null)
            return new Response(Fail, null, "There's no book with id="+id+"!");
        dao.remove(id);
        return new Response(Ok, null, null);
    }

    private Response edit (String[] args){
        if (HomeLib.getCurrentUser().getRole() != PersonRole.Administrator)
            return new Response(Fail, null, "Access denied!");
        if (args.length != 1)
            return new Response(Fail, null, "Require one parameter!");
        int id;
        try{
            id = Integer.parseInt(args[0]);
        } catch (Exception e) {
            return new Response(Fail, null, e.getMessage());
        }
        BookDao dao = BookDao.getInstance();
        Book book = dao.get(id);
        if (book == null)
            return new Response(Fail, null, "There's no book with id="+id+"!");
        return new Response(Redir, "editBook", book);
    }

    private Response add (){
        if (HomeLib.getCurrentUser().getRole() != PersonRole.Administrator)
            return new Response(Fail, null, "Access denied!");
        return new Response(Redir, "editBook", null);
    }

    /**
     * Method to handle 'come-back' type of command.
     * @return is the response that means coming back.
     */
    @Override
    public Response back() {
        return new Response(ResponseCode.Back, null, null);
    }
}
