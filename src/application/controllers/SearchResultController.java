package application.controllers;

import infrastructure.Response;
import infrastructure.ResponseCode;
import infrastructure.interfaces.IController;

/**
 * Controller that handles commands from search result view.
 */
public final class SearchResultController implements IController {

    /**
     * Method for handling a command, that comes as {@param command}.
     * Method returns {@return} response that can be handled by View-system.
     */
    @Override
    public Response handleCommand(String command) {
        if (command.equalsIgnoreCase("back"))
            return back();
        else
            return new Response(ResponseCode.Fail, null, command+"- is not a command!");
    }

    /**
     * Method to handle 'come-back' type of command.
     *
     * @return is the response that means coming back.
     */
    @Override
    public Response back() {
        return new Response(ResponseCode.Back, null, null);
    }
}
