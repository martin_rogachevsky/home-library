package application.controllers;

import application.HomeLib;
import domain.Person;
import domain.PersonRole;
import infrastructure.InputParser;
import infrastructure.InputValidator;
import infrastructure.interfaces.IController;
import infrastructure.Response;
import infrastructure.ResponseCode;
import infrastructure.dao.PersonDao;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;

/**
 * Controller that handles commands from log in view.
 */
public final class LogInController implements IController{

    /**
     * Method for handling a command, that comes as {@param command}.
     * Method returns {@return} response that can be handled by View-system.
     */
    @Override
    public Response handleCommand(String command){
        if (command.trim().equalsIgnoreCase("back"))
            return back();

        PersonDao personDao = PersonDao.getInstance();

        String login = InputParser.getLogin(command);
        String password = InputParser.getPassword(command);
        if (!InputValidator.isLoginValid(login))
            return new Response(ResponseCode.Fail,null, "Login is invalid!");
        if (!InputValidator.isPasswordValid(password))
            return new Response(ResponseCode.Fail, null,"Password is invalid!");

        try{
            password = new String(MessageDigest.getInstance("SHA-256").digest(password.getBytes(StandardCharsets.UTF_8))
                    , StandardCharsets.UTF_8);
        }catch (Exception e){
            return new Response(ResponseCode.Fail, null, "There's no SHA-256 algorithm.");
        }
        Person person = personDao.getPerson(login, password);
        if (personDao.getPerson(login, password) == null)
            return new Response(ResponseCode.Fail,null,  "Login and Password doesn't match!");
        else{
            HomeLib.setCurrentUser(person);
            return new Response(ResponseCode.Back, null, person);
        }
    }

    /**
     * Method to handle 'come-back' type of command.
     * @return is the response that means coming back.
     */
    @Override
    public Response back() {
        return new Response(ResponseCode.Back, null, null);
    }
}
