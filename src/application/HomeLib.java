package application;

import application.controllers.*;
import domain.*;
import infrastructure.*;
import infrastructure.dao.BookDao;
import infrastructure.dao.PersonDao;
import infrastructure.interfaces.IDao;
import infrastructure.interfaces.IView;
import presentation.*;
import presentation.views.*;

import java.util.HashSet;
import java.util.Stack;


/**
 * Main class of the application.
 */
public class HomeLib {
    private static Person currentUser;
    private static Stack<String> path;
    private static InputHandler handler;
    private static Viewer viewer;

    /**
     * Configuration of application components.
     * @throws Exception Can be caused by opening DAO connections.
     */
    private static void configure() throws Exception{
        //path configuration
        path = new Stack<>();
        path.push("lib");
        path.push("menu");

        //InputHandler configuration
        handler = new InputHandler();
        handler.addController("menu", new MenuController());
        handler.addController("catalog", new CatalogController());
        handler.addController("auth", new LogInController());
        handler.addController("sign", new SignUpController());
        handler.addController("editBook", new EditBookController());
        handler.addController("search", new SearchController());
        handler.addController("searchResult", new SearchResultController());

        //Viewer configuration
        viewer = new Viewer();
        viewer.addView("menu", new MenuView());
        viewer.addView("err", new ErrorView());
        viewer.addView("auth", new LogInView());
        viewer.addView("sign", new SignUpView());
        viewer.addView("editBook", new EditBookView());
        viewer.addView("catalog", new CatalogView());
        viewer.addView("search", new SearchView());
        viewer.addView("searchResult", new SearchRelustView());

        //Dao configuration
        PersonDao.getInstance().openConnection("people.txt");
        BookDao.getInstance().openConnection("books.txt");
    }

    /**
     * Start point of the application.
     * @param args Arguments from console.
     */
    public static void main(String[] args) {
        try{
            configure();
        }catch (Exception e){
            System.out.println("Configuration error!");
            return;
        }

        boolean stop = true;
        String input;
        Response response;
        IView view = viewer.getView(path.peek(),null);

        while (stop){
            input =  view.show();
            if (input.equalsIgnoreCase("quit")) stop = false;
            else{
                input = path.peek() + ";" + input;
                try{
                    response = handler.handleInput(input);
                    view = viewer.getView(response, path);
                }
                catch (Exception e){
                    viewer.getView("err", e).show();
                }
            }
        }
    }

    /**
     * Setter for currentUser.
     * @param person Person that supposed to be a new currentUser.
     */
    public static void setCurrentUser(Person person){
        currentUser = person;
    }

    /**
     *Getter for a currentUser.
     */
    public static Person getCurrentUser(){
        return currentUser;
    }
}
