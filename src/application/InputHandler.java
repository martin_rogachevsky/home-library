package application;

import infrastructure.interfaces.IController;
import infrastructure.Response;

import java.util.HashMap;
import java.util.Map;

/**
 * Class for handling requests from views.
 */
class InputHandler {
    private Map<String, IController> controllers;

    InputHandler(){
        controllers = new HashMap<>();
    }

    /**
     * Method that handles requests.
     * @param input Request from view.
     * @return Response gained in the controller.
     * @throws Exception Can be caused by invalid request.
     */
    Response handleInput(String input) throws Exception{
        if (input == null || input == "") throw new Exception("Input can not be empty!");

        String[] inputParts = input.trim().split(";",2);
        if (inputParts.length <= 1 || inputParts[1].trim().length() == 0) throw new Exception("Input can not be empty!");

        String path = inputParts[0];
        IController controller = controllers.getOrDefault(path, null);
        if (controller == null) throw new Exception("Controller for "+path+" doesn't set!");

        return controller.handleCommand(inputParts[1]);
    }

    /**
     * Method for connecting controllers to InputHandler.
     * @param name Name of the controller.
     * @param controller Controller object.
     */
    void addController(String name, IController controller){
        controllers.put(name, controller);
    }
}
