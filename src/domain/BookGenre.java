package domain;

/**
 * Enum that determines available book genres.
 */
public enum BookGenre {
    Comedy,
    Drama,
    HorrorFiction,
    LiteraryRealism,
    Romance,
    Satire,
    Tragedy,
    Tragicomedy,
    Fantasy,
    Mythology,
    Technology
}
