package domain;

/**
 * Enum that determines available person's roles.
 */
public enum PersonRole {
    Administrator,
    User
}