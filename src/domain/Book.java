package domain;

/**
 * Book domain class.
 */
public class Book extends Item{

    /**
     * Book title.
     */
    public String title;

    /**
     * Book author.
     */
    public String author;

    /**
     * Book genre.
     */
    public BookGenre genre;

    /**
     * Field that determines whether book is paper or eBook.
     * {@code true} - is eBook.
     * {@code false} - is a paper book.
     */
    public Boolean isEbook;

    public Book() {

    }

    public Book(String title, String author, BookGenre genre, Boolean isEbook){
        this.title = title;
        this.author = author;
        this.genre = genre;
        this.isEbook = isEbook;
    }
}
