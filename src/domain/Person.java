package domain;

/**
 * Person domain class.
 */
public class Person extends Item{

    /**
     * Person's name.
     */
    public String name;

    /**
     * Person's encrypted password.
     */
    public String password;

    /**
     * Person's email.
     */
    public String email;

    /**
     * Person's role.
     */
    private PersonRole role;

    public Person (PersonRole role){
        this.role = role;
    }

    /**
     * Person's role setter.
     * @param role - role that supposed to be set.
     */
    public void setRole (PersonRole role) {
        this.role = role;
    }

    /**
     * Person's role getter.
     * @return - person's role.
     */
    public PersonRole getRole(){
        return role;
    }
}
